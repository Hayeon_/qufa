host: 164.125.254.36 or 10.125.37.214
port: 5555
indicator: eqop, eqod, depa, treq
key: D3E7D9D8-24E9-4698-9D1F-5098C2EE081C
data: true, false (optional)

curl --request GET -i "http://10.125.37.214:5555/api/fa/health?key=D3E7D9D8-24E9-4698-9D1F-5098C2EE081C&indicator=eqop&data=false"
curl --request GET "http://10.125.37.214:5555/api/fa/health?key=D3E7D9D8-24E9-4698-9D1F-5098C2EE081C&indicator=eqop&data=false" | python3 -m json.tool
