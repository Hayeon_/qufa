from flask import Flask
from route.route_sample import bp_sample
from route.route_qufa3_fa import bp_qufa3_fa
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

app.register_blueprint(bp_sample)
app.register_blueprint(bp_qufa3_fa)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5555, debug=True)

