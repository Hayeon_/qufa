									원본		GAN			보정후
전체								101500		33495		12375
심장발병예측			heart_risk10
	심장발병확률낮음	0			60694		16693		5049
	심장발경확률높음	1			40806		16802		7326
성별					sex
	남					0			41557		20606		5895
	여					1			59943		12889		6480
연령대					age
	10대				0			651			141			146
	20대				1			9720		3403		1568
	30대				2			34468		8314		3088
	40대				3			21223		10177		2729
	50대				4			17073		3116		2120
	60대				5			13358		6364		1687
	70대				6			4528		1839		914
	80대				7			479			141			123
심장질환과거력			htn
	없음				0			84918		26592		8215
	있음				1			16582		6903		4160
심장질환가족력			fhtnyn
	없음				0			63083		24224		6655
	있음				1			38417		9271		5720
연평균흡연량			packyear
	_갑					0			8252		5202		2863
						1			10058		2732		2824
						2			2733		686			1441
						3			543			177			346
						4			126			22			82
						5			34			18			35
						6			19			23			31
						7			8			14			14
						8			3			30			25
						9			3			32			26
						10			4			34			26
						11			1			43			27
						12			1			39			28
						13			79715		24443		4607
일주일간음주빈도		sd_idr2
	_회					0			16087		3211		2052
						1			24783		7096		2284
						2			12691		5820		2044
						3			5719		1282		1497
						4			2823		1049		1128
						5			1229		553			643
						6			371			83			274
						7			61			22			54
						8			5			24			14
						9			37731		14355		2385
