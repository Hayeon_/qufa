package com.example.qufa3_isoft.controller;

import com.example.qufa3_isoft.repository.MainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MainController {
    @Autowired
    private MainRepository mainRepository;

    @RequestMapping(value = {"/getNow"}, method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getNow(HttpServletRequest request, Principal principal) {
        Map<String, Object> resultMap = new HashMap<>();
        Map<String, Object> paramMap = new HashMap<>();
        resultMap.put("now", mainRepository.getNow(paramMap));
        return resultMap;
    }

    @RequestMapping(value = {"/pw"}, method = RequestMethod.GET)
    @ResponseBody
    public String pw(HttpServletRequest request) {
        String resultString;
        String password = request.getParameter("password");
        if (password == null || password.length() == 0) {
            resultString = "no parameter";
        } else {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            resultString = encoder.encode(password);
            System.out.println("encode: " + resultString);
        }
        return resultString;
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String index(Model model) {
//        return "index";
        return "redirect:/main";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(Model model,
                        @RequestParam(value = "result", required = false) String result,
                        @RequestParam(value = "message", required = false) String message) {
        if (result == null) {
            model.addAttribute("result", "success");
        } else {
            model.addAttribute("result", result);
        }
        if (message == null) {
            model.addAttribute("message", "ok");
        } else {
            model.addAttribute("message", message);
        }
        return "/login/login";
    }

    @RequestMapping(value = {"/main"}, method = RequestMethod.GET)
    public String main(Model model) {
        return "/main/main";
    }

    @RequestMapping(value = {"/main/case1"}, method = RequestMethod.GET)
    public String mainCase1(Model model) {
        return "/main/case1/case1";
    }

    @RequestMapping(value = {"/main/case1/static"}, method = RequestMethod.GET)
    public String mainCase1Static(Model model) {
        return "/main/case1/static";
    }

    @RequestMapping(value = {"/main/case1/static/result"}, method = RequestMethod.GET)
    public String mainCase1StaticResult(Model model) {
        return "/main/case1/static_result";
    }

    @RequestMapping(value = {"/main/case1/dynamic"}, method = RequestMethod.GET)
    public String mainCase1Dynamic(Model model) {
        return "/main/case1/dynamic";
    }

    @RequestMapping(value = {"/main/case2"}, method = RequestMethod.GET)
    public String mainCase2(Model model) {
        return "/main/case2/case2";
    }

    @RequestMapping(value = {"/main/case2/static"}, method = RequestMethod.GET)
    public String mainCase2Static(Model model) {
        return "/main/case2/static";
    }

    @RequestMapping(value = {"/main/case2/dynamic"}, method = RequestMethod.GET)
    public String mainCase2Dynamic(Model model) {
        return "/main/case2/dynamic";
    }

}

